public class ArrayManipulation{
    //1. Create a new Java class called "ArrayManipulation".
    public static void main(String[] args) {
        //2. Inside the "ArrayManipulation" class, declare and initialize the following arrays:
        int [] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values= new double[4];
        double maximum =0;

        values[0]=215.22;
        values[1]=65.255;
        values[2]=45.226;
        values[3]=455.6554;

        int sum=0;

        //3. Print the elements of the "numbers" array using a for loop.
        for(int i=0;i<numbers.length;i++){
            System.out.println(numbers[i]);
        }
        //4.Print the elements of the "names" array using a for-each loop.
        System.out.println();
        for(String X:names){
            System.out.println(X);
        }

         System.out.println();
        //5. Calculate and print the sum of all elements in the "numbers" array.
        for(int i=0;i<numbers.length;i++){
            sum=sum+numbers[i];
        }
        System.out.println(sum);
        System.out.println();

        //6. Find and print the maximum value in the "values" array.
        
        for(int i=0;i<values.length;i++){
            if(values[i]>maximum){
                maximum=values[i];
            }
        }
        System.err.println(maximum);
        
        String [] reversedNames =new String[names.length];

        System.out.println();
         for(int i=0;i<reversedNames.length;i++){
            reversedNames[i]=names[(names.length)-(i+1)];
            System.out.println(reversedNames[i]);
        }

         
        //BONUS: Sort the "numbers" array in ascending order using any sorting algorithm of your choice.Print the sorted "numbers" array


        for (int i = 0; i < numbers.length - 1; i++) {
            for (int j = 0; j < numbers.length - i - 1; j++) {
                if (numbers[j] > numbers[j + 1]) {
                    // สลับค่า
                    int num = numbers[j];
                    numbers[j] = numbers[j + 1];
                    numbers[j + 1] = num;
                }
            }
        }

         for(int i=0;i<values.length;i++){
            System.out.println(numbers[i]);
        }

    }
     
    
 
}   

